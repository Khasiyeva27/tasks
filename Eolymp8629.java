import java.util.Scanner;

public class Eolymp8629 {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int n = sc.nextInt();

        int a=n/1000;
        int b=n/100%10;
        int c=n/10%10;
        int d=n%10;

        if(a%2==1 || b%2==1 || c%2==1 || d%2==1){
            System.out.println("YES");
        }
        else{
            System.out.println("NO");
        }
    }
}
