import java.util.Scanner;

public class Eolymp8893 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        if(n%3==0 && n%2==0 && (n>=10 && n<=99) || (n>=-99 && n<=-10)){
            System.out.println("YES");
        }
        else{
            System.out.println("NO");
        }
    }
}
