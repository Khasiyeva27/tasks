import java.util.Scanner;

public class Eolymp8628 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int a=n/1000;
        int b=n/100%10;
        int c=n/10%10;
        int d=n%10;

        if(a%2==0 && b%2==0 && c%2==0 && d%2==0){
            System.out.println("YES");
        }
        else{
            System.out.println("NO");
        }

    }
}
