import java.util.Scanner;

public class Eolymp8892 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long n = sc.nextLong();

        int sum=0;
        if(n%2!=0)  sum++;

        if(n>=100 && n<=999) sum++;

        if(sum>0)
            System.out.println("YES");
        else
            System.out.println("NO");

    }
}