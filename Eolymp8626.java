import java.util.Scanner;

public class Eolymp8626 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int a=n/1000;
        int b=n/100%10;
        int c=n/10%10;
        int d=n%10;

        if(a==3 && b==7 || b==3 && c==7 || c==3 && d==7){
            System.out.println("YES");
        }
        else {
            System.out.println("NO");
        }
    }
}
